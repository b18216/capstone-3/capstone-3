import React from "react";
import "../css/Header.css";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext } from "react";

export default function Header() {
  const { user } = useContext(UserContext);

  return (
    <div className="header">
      <div className="header__container">
        <div className="main__nav">
          <div className="nav__logo">
            <Link to="/">
              <img src="/img/logo6.png" className="logo" alt="logo" />
            </Link>
          </div>
          <Link to="/" className="nav__content link">
            HOME
          </Link>
          <Link to="/store" className="nav__content link">
            STORE
          </Link>

          {user.isAdmin === true ? (
            <Link to="/admin" className="nav__content link">
              ADMIN
            </Link>
          ) : (
            <Link to="/store" className="nav__content link" hidden>
              STORE
            </Link>
          )}

          {user.id !== null ? (
            <Link to="/logout" className="nav__content link">
              LOGOUT
            </Link>
          ) : (
            <Link to="/register" className="nav__content link">
              REGISTER
            </Link>
          )}
        </div>

        <div className="right__nav">
          {user.id !== null ? (
            <div className="header-option">
              <span className="header-OptLine1">
                <Link to="/userprofile" className="link">
                  Welcome back
                </Link>
              </span>
              <span className="header-OptLine2">
                <Link to="#" className="link">
                  {user.name}
                </Link>
              </span>
            </div>
          ) : (
            <div className="header-option">
              <span className="header-OptLine1">Hello, Guest</span>
              <span className="header-OptLine2">
                <Link to="/login" className="link">
                  Sign In
                </Link>
              </span>
            </div>
          )}
          <Link to="/checkout" className="header-cart link">
            <ShoppingCartOutlinedIcon className="shopping-cart" />
            <span className="header-Optline2 item-count"></span>
          </Link>
        </div>
      </div>
    </div>
  );
}
