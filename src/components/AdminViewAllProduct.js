import React, { useContext } from "react";
import "../css/AdminViewAllProduct.css";
import UserContext from "../UserContext";
import Swal from "sweetalert";
import { useNavigate } from "react-router-dom";
// import { Link } from "react-router-dom";

const AdminViewAllProduct = (prop) => {
  const { user } = useContext(UserContext);
  console.log(user);
  const { productProp } = prop;
  const { name, description, price, _id, stocksAvailable, isActive } =
    productProp;
  const history = useNavigate();

  function deactivate(_id) {
    fetch(
      `https://nameless-fortress-75166.herokuapp.com/products/deactivateProduct/${_id}`,
      {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
      });
    Swal("Product Deactivated", "", "info");
  }

  function activate(_id) {
    if (stocksAvailable <= 0) {
      Swal("Stock available is 0", "this product cannot be activated", "info");
    } else {
      fetch(
        `https://nameless-fortress-75166.herokuapp.com/activateProduct/${_id}`,
        {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      )
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
        });
      Swal("Product Deactivated", "", "success");
      return;
    }
  }
  return (
    <>
      <div className="viewAll">
        <div>
          <strong> Name: </strong>
          {name}
        </div>
        <div>
          <strong>Description: </strong>
          {description}
        </div>
        <div>
          <strong>Price: </strong>
          {price}
        </div>
        <div>
          <strong>Stock Available: </strong>
          {stocksAvailable}
        </div>
      </div>
      <div className="viewAll__buttons">
        <button
          className="viewAll_1btn"
          onClick={() => history(`/editproduct/${_id}`)}
        >
          Edit
        </button>
        {isActive ? (
          <button className="viewAll_1btn" onClick={() => deactivate(_id)}>
            Deactivate
          </button>
        ) : (
          <button className="viewAll_1btn" onClick={() => activate(_id)}>
            Activate
          </button>
        )}
      </div>
    </>
  );
};
export default AdminViewAllProduct;
