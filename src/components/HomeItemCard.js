import React from "react";
import { Card, ListGroup, ListGroupItem } from "react-bootstrap";
import "../css/HomeItemCard.css";

function HomeitemCard(props) {
  const { name, description, price, image, link } = props;
  return (
    <div>
      <Card className="home__cards" style={{ width: "10rem" }}>
        <Card.Img variant="top" src={image} />
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Text>{description}</Card.Text>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroupItem>{price}</ListGroupItem>
        </ListGroup>
        <Card.Body>
          <Card.Link href={link}>Check Item</Card.Link>
        </Card.Body>
      </Card>
    </div>
  );
}

export default HomeitemCard;
