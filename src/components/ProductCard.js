import React from "react";
import "../css/ProductCard.css";
import { Link } from "react-router-dom";
import {
  CardMedia,
  CardContent,
  CardActions,
  Typography,
  IconButton,
} from "@mui/material";

import { AddShoppingCart } from "@mui/icons-material";
// import { Card } from "react-bootstrap";

const ProductCard = (prop) => {
  const { productProp } = prop;
  const { name, description, price, _id, stocksAvailable } = productProp;
  return (
    <div className="productcard__container">
      <div className="classes__root">
        <CardContent>
          <CardMedia className="classes__media" title={name} />
          <div className="classes__cardcontent">
            <Typography variant="h6" gutterBottom>
              {name}
            </Typography>
            <Typography variant="h9" color="textSecondary" gutterBottom>
              {description}
            </Typography>
          </div>
        </CardContent>
      </div>
      <CardActions disableSpacing className="classes_cardaction">
        {stocksAvailable <= 0 ? (
          <div className="nostocks__btn">
            <p>(Out of Stock)</p>
            <Link to={`/viewproduct/${_id}`}>Check Item</Link>
          </div>
        ) : (
          <>
            <Typography variant="h6">P{price}</Typography>
            <IconButton>
              <AddShoppingCart onClick={""} />
            </IconButton>
            <Link to={`/viewproduct/${_id}`}>Check Item</Link>
          </>
        )}
      </CardActions>
    </div>
  );
};
export default ProductCard;
