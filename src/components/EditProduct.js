import React, { useState, useContext, useEffect } from "react";
import UserContext from "../UserContext";
import { useNavigate, useParams } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert";
// import "../css/ViewProducts.css";

function EditProduct() {
  const { user } = useContext(UserContext);
  console.log(user);
  const history = useNavigate();

  const { productId } = useParams();
  // const history = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [image, setImage] = useState("");

  useEffect(() => {
    fetch(`https://nameless-fortress-75166.herokuapp.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setStock(data.stocksAvailable);
        setImage(data.image);
      });
  }, [productId]);

  // directing product using product id
  function updateProduct(e) {
    e.preventDefault();
    fetch(
      `https://nameless-fortress-75166.herokuapp.com/updateProduct/${productId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          name: name,
          description: description,
          price: price,
          stocksAvailable: stock,
          image: image,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
      });
    Swal("Update Successful", "", "success");
    history("/admin");
  }
  return (
    <>
      <h1>Update Product Details:</h1>
      <Form onSubmit={(e) => updateProduct(e)}>
        <Form.Group controlId="firstName">
          <Form.Label>Name:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Update for Name"
            required
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="description">
          <Form.Label>Description:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Please input your last name here"
            required
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="price">
          <Form.Label>Price:</Form.Label>
          <Form.Control
            type="number"
            placeholder="price"
            required
            value={price}
            onChange={(e) => setPrice(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="stock">
          <Form.Label>Stock Available:</Form.Label>
          <Form.Control
            type="number"
            placeholder="available stock"
            required
            value={stock}
            onChange={(e) => setStock(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="stock">
          <Form.Label>Image:</Form.Label>
          <Form.Control
            type="string"
            placeholder="Image URL"
            required
            value={image}
            onChange={(e) => setImage(e.target.value)}
          />
        </Form.Group>

        <Button
          variant="success"
          type="submit"
          id="submitBtn"
          className="mt-3 mb-5"
        >
          Update
        </Button>
        <Button
          variant="success"
          id="submitBtn"
          className="mt-3 mb-5"
          onClick={() => history("/admin")}
        >
          Back
        </Button>
      </Form>
    </>
  );
}
export default EditProduct;
