import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { useEffect, useState } from "react";
import { UserProvider } from "./UserContext";
import { Container } from "react-bootstrap";
import Header from "./components/Header";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Error from "./pages/Error";
import Logout from "./pages/Logout";
import Admin from "./pages/Admin";
import Store from "./pages/Store";
import ViewProduct from "./pages/ViewProduct";
import ViewAllProducts from "./pages/ViewAllProducts";
import AddNewProduct from "./pages/AddNewProduct";
import EditProduct from "./components/EditProduct";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch("https://nameless-fortress-75166.herokuapp.com/accounts/getDetails", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
            name: data.firstName,
          });
        } else {
          setUser({ id: null, isAdmin: null });
        }
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Header />
        <Container>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/store" element={<Store />} />
            <Route
              exact
              path="/viewproduct/:productId"
              element={<ViewProduct />}
            />
            {/* checkout */}
            {/* payment */}

            <Route
              exact
              path="/adminviewallproduct"
              element={<ViewAllProducts />}
            />
            <Route
              exact
              path="/adminaddnewproduct"
              element={<AddNewProduct />}
            />
            <Route
              exact
              path="/editproduct/:productId"
              element={<EditProduct />}
            />
            <Route exact path="/admin" element={<Admin />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="*" element={<Error />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/register" element={<Register />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
