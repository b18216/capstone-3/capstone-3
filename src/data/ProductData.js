const data = {
  products: [
    {
      id: "1",
      name: "Arabica",
      description: "sample description",
      price: 500,
      image: "../img/prdct_1.png",
    },
    {
      id: "2",
      name: "Robusta",
      description: "sample description",
      price: 500,
      image: "../img/prdct_2.png",
    },
    {
      id: "3",
      name: "Arabica Robusta",
      description: "sample description",
      price: 500,
      image: "../img/prdct_3.png",
    },
  ],
};
export default data;
