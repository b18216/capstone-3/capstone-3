import React, { useEffect, useState } from "react";
import "../css/Store.css";
import "../data/ProductData";
import ProductCard from "../components/ProductCard";
// import SearchIcon from "@mui/icons-material/Search";

export default function Store({ product }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch("https://nameless-fortress-75166.herokuapp.com/products")
      .then((res) => res.json())
      .then((data) => {
        const productArr = data.map((product) => {
          return <ProductCard key={product._id} productProp={product} />;
        });
        setProducts(productArr);
      });
  }, [product]);

  return (
    <div className="store">
      <div className="store__container"> {products}</div>
    </div>
  );
}
