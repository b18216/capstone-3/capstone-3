import React, { useContext, useEffect, useState } from "react";
import { Link, Navigate } from "react-router-dom";
import "../css/Signin.css";
import UserContext from "../UserContext";
import Swal from "sweetalert";
import { Button } from "react-bootstrap";

export default function Login() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(true);

  const validate = (e) => {
    e.preventDefault();

    fetch("https://nameless-fortress-75166.herokuapp.com/accounts/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.accesstoken !== "undefined") {
          localStorage.setItem("token", data.accesstoken);
          retrieveUserDetails(data.accesstoken);
          Swal("Login Complete", "Welcome", "success");
        } else if (email !== true || password !== true) {
          Swal("Login failed!", "Please try again", "error");
        }
      });
    setEmail("");
    setPassword("");
  };

  // IF USER IS ADMIN
  const retrieveUserDetails = (token) => {
    fetch("https://nameless-fortress-75166.herokuapp.com/accounts/getDetails", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };
  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <>
      <form className="signin" onSubmit={(e) => validate(e)}>
        <div className="signin__form">
          <h3 className="signin_title">SIGN IN</h3>
          <input
            className="signin__input"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Email"
          />
          <input
            className="signin__input"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Password"
            type="password"
          />

          <Link to="/forgotpassword" className="signin__forgotps">
            Forgot your password?
          </Link>
        </div>

        {isActive ? (
          <Button variant="success" className="singin__button" type="submit">
            Sign In
          </Button>
        ) : (
          <Button
            variant="success"
            className="singin__button"
            type="submit"
            disabled
          >
            Sign In
          </Button>
        )}

        <p>
          Not yet a member?
          <Link className="link" to="/register">
            - Register
          </Link>
        </p>
      </form>
    </>
  );
}
