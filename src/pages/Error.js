import React from "react";
import { Link } from "react-router-dom";
import "../css/Error.css";

function Error() {
  return (
    <div className="error">
      <div className="error__container">
        <h1 className="error__404">ERROR: 404</h1>
        <p className="error__txt">Page is do not exist</p>

        <p className="error__redirect">
          Return to -
          <Link className="error__link" to="/">
            Home Page
          </Link>
        </p>
      </div>
    </div>
  );
}

export default Error;
