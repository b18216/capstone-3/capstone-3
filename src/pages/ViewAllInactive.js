import React, { useEffect, useState } from "react";
import "../css/Store.css";
import "../data/ProductData";
import AdminViewAllProduct from "../components/AdminViewAllProduct";
// import CartItems from "../components/CartItems";
// import SearchIcon from "@mui/icons-material/Search";

export default function ViewAllInActive({ product }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(
      "https://nameless-fortress-75166.herokuapp.com/products/getAllInactive/Inactive",
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        const productArr = data.map((product) => {
          return (
            <AdminViewAllProduct key={product._id} productProp={product} />
          );
        });
        setProducts(productArr);
      });
  }, [product]);

  return (
    <div className="store">
      <div className="store__container"> {products}</div>
    </div>
  );
}
