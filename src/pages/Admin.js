import React, { useContext, useState } from "react";
import { Link, Navigate } from "react-router-dom";
import Accordion from "react-bootstrap/Accordion";
import "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../css/Admin.css";
import AddNewProduct from "./AddNewProduct";
import ViewAllProducts from "./ViewAllProducts";
import ViewAllActive from "./ViewAllActive";
import ViewAllInactive from "./ViewAllInactive";
import UserContext from "../UserContext";
// import { Button } from "react-bootstrap";

// import { Button } from "react-bootstrap";

function Admin() {
  const { user } = useContext(UserContext);

  const [reg, setReg] = useState(!true);
  const [edit, setEdit] = useState(!true);
  const [active, setActive] = useState(!true);
  const [inactive, setInActive] = useState(!true);
  console.log(reg, edit);

  console.log(reg, edit, active, inactive);

  return (
    <>
      {user.isAdmin ? (
        <div className="admin">
          <div className="admin__container">
            <Accordion eventKey="0">
              <h4 className="admin__header">ADMIN TOOLS</h4>
              <Accordion.Item eventKey="0">
                <Accordion.Header>Products</Accordion.Header>
                <Accordion.Body
                  id="accdn_prdct"
                  className="accordion__products"
                >
                  <button
                    className="admin__link"
                    onClick={() => {
                      setReg(!true);
                      setEdit(true);
                      setActive(!true);
                      setInActive(!true);
                    }}
                  >
                    View All Products
                  </button>
                  <hr></hr>
                  <button
                    className="admin__link"
                    onClick={() => {
                      setReg(!true);
                      setEdit(!true);
                      setActive(true);
                      setInActive(!true);
                    }}
                  >
                    View Active Products
                  </button>

                  <hr></hr>
                  <button
                    className="admin__link setReg"
                    onClick={() => {
                      setReg(!true);
                      setEdit(!true);
                      setActive(!true);
                      setInActive(true);
                    }}
                  >
                    View All Inactive Products
                  </button>
                  <hr></hr>
                  <button
                    className="admin__link setReg"
                    onClick={() => {
                      setReg(true);
                      setEdit(!true);
                      setActive(!true);
                      setInActive(!true);
                    }}
                  >
                    Add New Product
                  </button>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="1">
                <Accordion.Header>User Accounts</Accordion.Header>
                <Accordion.Body>
                  <Link className="admin__link" to="*">
                    View All User
                  </Link>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="2">
                <Accordion.Header>Sales Report</Accordion.Header>
                <Accordion.Body>
                  <Link className="admin__link" to="*">
                    Coming Soon
                  </Link>
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </div>
          <div className="admin__container2">
            <div className="admin__components">
              {reg && (
                <>
                  <h4 className="view_title"> Register New Product</h4>
                  <AddNewProduct />
                </>
              )}
              {edit && (
                <>
                  <h4 className="view_title"> All Products</h4>
                  <ViewAllProducts />
                </>
              )}
              {active && (
                <>
                  <h4 className="view_title">All Active Products</h4>
                  <ViewAllActive />
                </>
              )}
              {inactive && (
                <>
                  <h4 className="view_title">All Inactive Products</h4>
                  <ViewAllInactive />
                </>
              )}
            </div>
          </div>
        </div>
      ) : (
        <Navigate to="/" />
      )}
    </>
  );
}

export default Admin;
