import React, { useState, useContext, useEffect } from "react";
import UserContext from "../UserContext";
import { useParams, useNavigate } from "react-router-dom";
import { Container, Row, Card, Col, Button } from "react-bootstrap";
import "../css/ViewProducts.css";
import Swal from "sweetalert";

export default function ViewProduct() {
  const { user } = useContext(UserContext);
  const history = useNavigate();

  const { productId } = useParams();
  // const history = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [image, setImage] = useState("");
  const [active, setActive] = useState(true);
  const [cart, setCart] = useState([]);

  // directing product using product id
  useEffect(() => {
    fetch(`https://nameless-fortress-75166.herokuapp.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setStock(data.stocksAvailable);
        setActive(data.isActive);
        setImage(data.image);
      });
  }, [productId]);

  const addBasket = () => {
    if (stock === 0 || active !== true) {
      Swal("Purchase failed", "Out of Stock/Not Available", "info");
      return;
    } else if (user.isAdmin) {
      Swal("Purchase failed", "Action not allowed for admin", "error");
      return;
    } else {
      setCart([...cart, { name, price, productId }]);
      console.log(cart);
    }
  };
  return (
    <div className="viewProduct">
      <Container className="mt-5">
        <Row>
          <Col lg={{ span: 6, offset: 3 }}>
            <Card>
              <Card.Body>
                <Card.Img variant="top" src={image} />
                <Card.Title>{name}</Card.Title>

                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>

                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>

                {/* CHECK IF LOGGED IN */}

                {user.id !== null ? (
                  <>
                    <Button
                      itemID="btnID"
                      variant="success"
                      className="viewproduct__btn"
                      onClick={() => addBasket(productId)}
                    >
                      Buy
                    </Button>
                    <Button
                      itemID="btnID"
                      variant="success"
                      className="viewproduct__btn"
                      onClick={() => history("/store")}
                    >
                      Back
                    </Button>
                  </>
                ) : (
                  <Button
                    itemID="btnID"
                    variant="success"
                    className="viewproduct__btn"
                    onClick={() => history("/login")}
                  >
                    Login
                  </Button>
                )}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
