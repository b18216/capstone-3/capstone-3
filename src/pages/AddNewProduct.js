import React, { useContext, useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert";
import "../css/AddNewProduct.css";
import { Button } from "react-bootstrap";

export default function AddNewProduct() {
  const { user } = useContext(UserContext);

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState();
  const [stock, setStock] = useState();
  const [image, setImage] = useState("");
  const [state, setState] = useState(false);

  useEffect(() => {
    if (name !== "" && description !== "" && price !== "" && stock !== "") {
      setState(true);
    } else {
      setState(false);
    }
  }, [name, description, price, stock]);

  function newProduct(e) {
    e.preventDefault();

    fetch(
      "https://nameless-fortress-75166.herokuapp.com/products/regNewProduct",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          name: name,
          price: price,
          description: description,
          image: image,
          stocksAvailable: stock,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
      });
    setName("");
    setDescription("");
    setPrice("");
    setStock("");
    setImage("");
    Swal("New Product Added", "", "success");
  }

  function clearField() {
    setName("");
    setDescription("");
    setPrice("");
    setStock("");
  }
  return user.isAdmin !== true ? (
    <Navigate to="/" />
  ) : (
    <div>
      <form className="addProduct">
        <div className="addProduct__container">
          <input
            className="addProduct__input"
            type="text"
            placeholder="Product Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <input
            className="addProduct__input"
            type="text"
            placeholder="Description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
          <input
            className="addProduct__input"
            type="Number"
            placeholder="Set Price"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
          />
          <input
            className="addProduct__input"
            type="text"
            placeholder="Image URL"
            value={image}
            onChange={(e) => setImage(e.target.value)}
          />
          <input
            className="addProduct__input"
            type="Number"
            placeholder="Stock Available"
            value={stock}
            onChange={(e) => setStock(e.target.value)}
          />
        </div>
        {state ? (
          <Button
            type="submit"
            variant="success"
            className="addProduct__button"
            onClick={(e) => newProduct(e)}
          >
            Submit
          </Button>
        ) : (
          <Button
            type="submit"
            variant="success"
            className="addProduct__button"
            onClick={(e) => newProduct(e)}
            disabled
          >
            Submit
          </Button>
        )}
        <Button
          type="submit"
          variant="success"
          className="addProduct__button"
          onClick={clearField}
        >
          Back
        </Button>
      </form>
    </div>
  );
}
