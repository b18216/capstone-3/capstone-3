import React, { useContext, useEffect, useState } from "react";
import "../css/Register.css";
import { Link, Navigate, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert";
import { Button } from "react-bootstrap";

export default function Register() {
  const { user } = useContext(UserContext);
  const history = useNavigate();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confPassword, setConfPassword] = useState("");
  const [isActive, setIsActive] = useState(false);
  console.log(user);

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      mobileNo.length >= 11 &&
      email !== "" &&
      password !== "" &&
      password === confPassword
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, mobileNo, email, password, confPassword]);

  function newUser(e) {
    e.preventDefault();

    fetch(
      "https://nameless-fortress-75166.herokuapp.com/accounts/isEmailAvailable",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email: email }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal(
            "Registration failed",
            "Email not available. try again.",
            "error"
          );
        } else {
          fetch("http://localhost:4000/accounts/register", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              mobileNo: mobileNo,
              email: email,
              password: password,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);

              if (data.email) {
                Swal("Registration successful", "Welcome!", "success");
                history("/");
              } else {
                Swal(
                  "Registration failed",
                  "Something went wrong. try again.",
                  "error"
                );
              }
            });
        }
      });

    setFirstName("");
    setLastName("");
    setEmail("");
    setMobileNo("");
    setPassword("");
    setConfPassword("");
  }
  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <form className="register">
      <div className="register__form">
        <h3 className="register__title">CREATE ACCOUNT</h3>
        <div className="register__name reg_flex">
          <input
            className="register_input_fname register__input"
            type="text"
            placeholder="First name"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />
          <input
            className="register_input_lname register__input"
            type="text"
            placeholder="Last Name"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          />
        </div>
        <div className="register__contactNo reg_flex">
          <input
            className="register_input"
            type="text"
            placeholder="Contact Number"
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
          />
        </div>
        <div className="register__email reg_flex">
          <input
            className="register_input"
            type="email"
            placeholder="Email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>

        <div className="register__password reg_flex">
          <input
            className="register_input"
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>

        <div className="register__confpass reg_flex">
          <input
            className="register_input"
            type="password"
            value={confPassword}
            placeholder="Confirm password"
            onChange={(e) => setConfPassword(e.target.value)}
          />
        </div>
      </div>
      {isActive ? (
        <Button
          variant="success"
          type="submit"
          className="register__button"
          onClick={(e) => newUser(e)}
        >
          Submit
        </Button>
      ) : (
        <Button
          variant="success"
          type="submit"
          className="register__button"
          disabled
        >
          Submit
        </Button>
      )}

      <p>
        Already a member?
        <Link className="link" to="/login">
          -Login
        </Link>
      </p>
    </form>
  );
}
