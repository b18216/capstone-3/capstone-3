import React from "react";
import { Link } from "react-router-dom";
import HomeitemCard from "../components/HomeItemCard";
// import ProductCard from "../components/ProductCard";
import "../css/Home.css";

const Home = () => {
  return (
    <>
      <div className="home">
        <div className="home__container">
          <Link to="/store">
            <img src="../img/banner.png" alt="" className="banner" />
          </Link>
        </div>
        <div className="home__lower">
          <h3>Try our Best Sellers!</h3>
          <div className="home__bestsellers">
            <HomeitemCard
              name="Café Lia Coffee - Arabica"
              description="Note:Lemony, Tangerine, Medium Roast"
              price="P350"
              image="../img/prdct_1.png"
              link="/viewproduct/62bc39a3385d5ee8917d228c"
            />
            <HomeitemCard
              name="Café Lia Coffee - Blend"
              description="20% Arabica + 80%Robusta, Medium Roast"
              price="P250"
              image="../img/prdct_2.png"
              link="/viewproduct/62bc3968385d5ee8917d2286"
            />
            <HomeitemCard
              name="Café Lia Coffee - Robusta"
              description="Note: Nutty, Caramel, Medium Roast "
              price="P200"
              image="../img/prdct_3.png"
              link="/viewproduct/62b5a6d58f13e0f8f28bf037"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
