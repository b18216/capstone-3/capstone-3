import React, { useContext, useEffect } from "react";
// import { useState } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert";

export default function Logout() {
  const { setUser, unsetUser } = useContext(UserContext);
  Swal("Logout Complete", "Visit us again", "success");

  unsetUser();
  useEffect(() => {
    setUser({
      id: null,
      isAdmin: null,
    });
  });

  return <Navigate to="/" />;
}
